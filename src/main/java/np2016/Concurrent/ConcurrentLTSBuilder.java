/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np2016.Concurrent;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import np2016.CCSSemantics.LTS;
import np2016.CCSSemantics.State;
import np2016.CCSSemantics.Transition;
import np2016.Graph.Graph;
import np2016.GraphSearch.LTSBuilder;

/**
 * Concurrent implementation of the {@link LTSBuilder}. This class pretty much
 * does the same job, as the SequentialLTSBuilder, but all methods are
 * implicitly locked. Thats because the inner laying lts is the shared memory
 * model, thats getting accessed from the {@link ConcurrentBFSGraphSearcher}
 * Threads.
 *
 * @see LTSBuilder
 */
public final class ConcurrentLTSBuilder extends LTSBuilder {

    // stores all nodes visited so far
    private final Set<State> visited;
    // stores all nodes that still need processing
    private final Queue<State> todo;

    /**
     * typical constructor, nothing special
     */
    public ConcurrentLTSBuilder() {
        super();
        this.visited = new HashSet<>();
        this.todo = new LinkedList<>();
    }

    /*
    All the following classes are synchronized, since this class is the main shared memory base, which tries to assure, that there are no dataraces on the lts.
     */
    /**
     * This method returns true if all nodes in the todo have been processed. It
     * is an essential part of the termination routine in the
     * {@link ConcurrentBFSGraphSearchManager}.
     *
     * @return if the todo list is empty, so the exploration of the lts is done
     */
    @Override
    public synchronized boolean done() {
        return this.todo.isEmpty();
    }

    /**
     * This method adds one state to the todo list.
     *
     * @param n the Node (or State) that should be put into the todo list
     */
    @Override
    public synchronized void offer(State n) {
        this.todo.offer(n);
    }

    /**
     * This method returns the next State in the todo list.
     *
     * @return the next State in the todo list
     */
    @Override
    public synchronized State peek() {
        return this.todo.peek();
    }

    /**
     * This method returns the next State in the todo list and removes it from
     * the todo list.
     *
     * @return the next State in the todo list
     */
    @Override
    public synchronized State poll() {
        return this.todo.poll();
    }

    /**
     * This method checks if the given state has been added to the visited list
     * already(so it has been seen before).
     *
     * @param state the state that should be checked
     * @return true, if the given state is visited already
     */
    @Override
    public synchronized boolean visited(State state) {
        return this.visited.contains(state);
    }

    /**
     * This method visits the given state (so it adds it to the visited list)
     *
     * @param graph this parameter is useless, but we didn't want to change the
     * already given interfaces :)
     * @param state the state that should be marked as visited
     */
    @Override
    public synchronized void discoverVertex(final Graph<State, Transition> graph,
            final State state) {
        visited.add(state);
    }

    /**
     * same as before but synchronized to assure, that no data races accure on
     * the lts
     *
     * @param graph
     * @param state
     */
    @Override
    public synchronized void startVertex(final Graph<State, Transition> graph,
            final State state) {
        this.lts = new LTS(state);
    }

    /**
     * same as before but synchronized to assure, that no data races accure on
     * the lts
     *
     * @param graph
     * @param transition
     */
    @Override
    public synchronized void nonTreeEdge(final Graph<State, Transition> graph,
            final Transition transition) {
        this.lts.addTransition(transition);
    }

    /**
     * same as before but synchronized to assure, that no data races accure on
     * the lts
     *
     * @param graph
     * @param transition
     */
    @Override
    public synchronized void treeEdge(final Graph<State, Transition> graph,
            final Transition transition) {
        this.lts.addState(transition.getTarget());
        this.lts.addTransition(transition);
    }

    /**
     * Since we wanted to reduce redundancy in the
     * {@link ConcurrentBFSGraphSearcher}, we transferred the functionality to
     * add an Edge to here. It autonomically decides if the given transition is
     * a treeEdge or a nonTreeEdge and calls the corresponding functions and all
     * the needed steps to process this one Edge in the LTS.
     *
     * @param graph
     * @param transition
     */
    @Override
    public synchronized void addEdge(final Graph<State, Transition> graph,
            final Transition transition) {
        if (!this.visited(transition.getTarget())) {
            this.discoverVertex(graph, transition.getTarget());
            this.treeEdge(graph, transition);
            this.offer(transition.getTarget());
        } else {
            this.nonTreeEdge(graph, transition);
        }
    }
}
