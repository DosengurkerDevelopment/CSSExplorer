/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np2016.Concurrent;

import java.util.List;
import np2016.Graph.Edge;
import np2016.Graph.Graph;
import np2016.Graph.Node;
import np2016.GraphSearch.BFSGraphSearch;
import np2016.GraphSearch.BFSGraphVisitor;

/**
 *
 * @author Daniel
 */
/**
 * Concurrent BFS (breadth-first-search) graph search implementation. Uses a
 * {@link ConcurrentLTSBuilder} to tell the progress and happenings of the
 * search. This is the "worker" and receives work from the manager.
 *
 * @param <N> the node type of the graph that is being searched.
 * @param <E> the edge type of the graph that is being searched.
 */
public class ConcurrentBFSGraphSearcher<N extends Node<?>, E extends Edge<N, ?>> extends BFSGraphSearch<N, E> {

    private SearcherState state;
    private Graph<N, E> graph;
    private final ConcurrentBFSGraphSearchManager<N, E> manager;

    /**
     * Instantiates the searcher with a GraphVisitor and the related manager
     *
     * @param manager the manager instance this is being started from
     * @param visitor in this case this is a {@link ConcurrentLTSBuilder} to
     * build the LTS
     * @see BFSGraphSearch
     */
    public ConcurrentBFSGraphSearcher(ConcurrentBFSGraphSearchManager<N, E> manager, BFSGraphVisitor<N, E> visitor) {
        super(visitor);
        this.manager = manager;
        this.state = SearcherState.Started;
    }

    /**
     * @return a {@link SearcherState} to find out, whether the Thread is
     * waiting to acquire a new ticket, working or just started.
     */
    public SearcherState getSearcherState() {
        return this.state;
    }

    /**
     * Sets the graph that needs to be searched.
     *
     * @param graph the graph to be set
     */
    public void setGraph(Graph<N, E> graph) {
        this.graph = graph;
    }

    /**
     * This method is the routine, that searches one node of the given graph,
     * adds new found nodes to the lts via the visitor
     * ({@link ConcurrentLTSBuilder}) and lists new nodes in the visitor. It is
     * called in this classes run method every time for each node in the todo
     * list of the visitor.
     *
     * @param graph
     * @param vertex
     */
    @Override
    public void search(Graph<N, E> graph, N vertex) {
        //System.out.println(this.getName());
        // get the first node in the queue and generate the outgoing edges
        // add the node to the visited set
        List<E> edges = graph.getEdges(vertex);
        if (edges.isEmpty()) {
            visitor.discoverVertex(graph, vertex);
        } else // more than one edge
        {
            for (E edge : edges) {
                visitor.addEdge(graph, edge);
                manager.release();
            }
        }
    }

    /**
     * This is the main work routine of the Searcher Threads. It tries to
     * acquire a ticket from the manager (only possible, when work is present,
     * so the 'todo'-list is not empty, because of the semaphore in the
     * manager), then gets a node of the 'todo'-list of the visitor and uses its
     * search method to process this node. This also sets the corresponding
     * {@link SearcherState} of this worker and periodically checks if it needs
     * to be interrupted.
     */
    @Override
    public void run() {
        while (!this.isInterrupted()) {

            this.state = SearcherState.Waiting;
            //wake up manager to check, if 'todo'-list is empty and all Threads are waiting
            synchronized (visitor) {
                visitor.notifyAll();
            }
            /* wait for a Ticket to be released.
            while the Thread is waiting for a new Ticket, it is suspended */
            manager.acquire();
            //set state to 'Busy' because ticket was acquired and searcher can now start working
            this.state = SearcherState.Busy;
            N next = visitor.poll();
            if (next != null) {
                search(graph, next);
            }
        }
    }

}
