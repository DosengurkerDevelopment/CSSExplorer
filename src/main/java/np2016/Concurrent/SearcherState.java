/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np2016.Concurrent;

/**
 *
 * @author daniel
 */
public enum SearcherState {
    /**
     * searcher has been run and is working
     */
    Busy,
    /**
     * searcher has been run but has no work currently
     */
    Waiting,
    /**
     * searcher started, but is not yet waiting or working (not run yet)
     */
    Started;
}
