/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np2016.Concurrent;

import java.util.ArrayList;
import java.util.List;
import np2016.CCSExplorer;
import np2016.Graph.Edge;
import np2016.Graph.Graph;
import np2016.Graph.Node;
import np2016.GraphSearch.BFSGraphSearch;
import np2016.GraphSearch.BFSGraphVisitor;

/**
 *
 * @author Daniel
 */
/**
 * Concurrent BFS (breadth-first-search) graph search manager class. This is
 * running in the main Thread and manages the {@link ConcurrentBFSGraphSearcher}
 * Threads concurrency. It also implements a Semaphore, that holds as many
 * tickets as there are nodes that need further procession.
 *
 * @param <N> the node type of the graph that is being searched.
 * @param <E> the edge type of the graph that is being searched.
 */
public class ConcurrentBFSGraphSearchManager<N extends Node<?>, E extends Edge<N, ?>>
        extends BFSGraphSearch<N, E> {

    /* this holds the amount of tickets to give away. It should be always the same amount as available nodes in the todo list. 
    Together with the aquire and release method of this class, this forms a Semaphore*/
    private int tickets;
    // this is a list of all worker Threads we need.
    private final List<ConcurrentBFSGraphSearcher<N, E>> searchers;
    // count of Threads we need.
    private final int THREADS;
    // The given Graph to be explored
    private Graph<N, E> graph;

    /**
     *
     * Constructor for this class. It declares the tickets with 0 and creates as
     * many Threads ({@link ConcurrentBFSGraphSearcher}) as needed.
     *
     * @param visitor This is the visitor used to fill up the resulting LTS
     * Graph. In this class you usually use a {@link ConcurrentLTSBuilder}
     * @param THREADS Used to determine the number of Threads that should be
     * used
     */
    public ConcurrentBFSGraphSearchManager(BFSGraphVisitor<N, E> visitor, int THREADS) {
        super(visitor);
        this.THREADS = THREADS;
        this.searchers = new ArrayList<>();
        this.tickets = 0;
        for (int i = 0; i < THREADS; i++) {
            ConcurrentBFSGraphSearcher searcher = (new ConcurrentBFSGraphSearcher<>(this, this.visitor));
            searchers.add(searcher);
        }
    }

    /**
     * This method acquires a ticket of the intern "Semaphore" if enough tickets
     * are available, otherwise it sets the requesting worker in the wait set of
     * the manager. It is synchronized , since it holds read and writes on the
     * shared memory variable this.tickets!
     */
    public synchronized void acquire() {
        int h;
        h = this.tickets;
        while (h <= 0) {
            try {
                this.wait();
                h = this.tickets;
            } catch (InterruptedException e) {
                //System.out.println("error in aquire");
            }
        }
        this.tickets--;
    }

    /**
     * This method releases a ticket of the intern "Semaphore", which is always
     * possible. It is synchronized , since it holds read and writes on the
     * shared memory variable this.tickets! It notifies all waiting workers to
     * check if tickets are available again.
     */
    public synchronized void release() {
        this.tickets++;
        this.notifyAll();
    }

    /**
     * This method is called in the {@link CCSExplorer} (main class) and starts
     * as many concurrent {@link ConcurrentBFSGraphSearcher} as the THREADs
     * value given in the constructor after it puts the startVertex of the Graph
     * in the to do list of the visitor({@link ConcurrentLTSBuilder}). After
     * starting the Threads it waits(in its own waitSet) until it gets a
     * terminated signal of one of the Threads and looks up if all the Searcher
     * Threads are in a WAITING state ({@link SearcherState}) and no work needs
     * to be done anymore(the todo list of the visitor is empty). After this
     * condition is given it terminates the Searcher Threads and leaves this
     * method.
     *
     * @param graph The graph that needs to be searched through
     * @param startVertex this is the root(startNode) of the given graph
     */
    @Override
    public void search(Graph<N, E> graph, N startVertex) {
        // handle the start node
        this.visitor.startVertex(graph, startVertex);
        this.visitor.discoverVertex(graph, startVertex);
        visitor.offer(startVertex);
        this.release();
        //start Searcher-Threads
        for (ConcurrentBFSGraphSearcher searcher : searchers) {
            searcher.setGraph(graph);
            searcher.start();
        }
        // wait for finished signal
        boolean terminate = false;
        synchronized (visitor) {
            while (!terminate) {
                try {
                    visitor.wait();
                } catch (InterruptedException ex) {
                    //System.out.println("wait interrupted");
                }
                terminate = visitor.done();
                //check if all Threads are in state "Waiting", i.e. all Threads are waiting to acquire a new ticket from the Semaphore
                for (ConcurrentBFSGraphSearcher searcher : searchers) {
                    terminate &= searcher.getSearcherState() == SearcherState.Waiting;
                }

            }
        }

        // terminate Threads and wait for termination
        for (ConcurrentBFSGraphSearcher searcher : searchers) {
            searcher.interrupt();
        }
        //release tickets, because every Searcher-Thread needs a ticket to continue and finally terminate
        for (ConcurrentBFSGraphSearcher searcher : searchers) {
            this.release();
        }
    }

}
