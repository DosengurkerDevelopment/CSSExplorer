package np2016.GraphSearch;

import np2016.CCSSemantics.LTS;
import np2016.CCSSemantics.State;
import np2016.CCSSemantics.Transition;
import np2016.Graph.Graph;

/**
 * Sequential implementation of the {@link LTSBuilder}.
 *
 * @see LTSBuilder
 */
public final class SequentialLTSBuilder extends LTSBuilder {

    @Override
    public void startVertex(final Graph<State, Transition> graph,
            final State state) {
        this.lts = new LTS(state);
    }

    @Override
    public void nonTreeEdge(final Graph<State, Transition> graph,
            final Transition transition) {
        this.lts.addTransition(transition);
    }

    @Override
    public void treeEdge(final Graph<State, Transition> graph,
            final Transition transition) {
        this.lts.addState(transition.getTarget());
        this.lts.addTransition(transition);
    }

    @Override
    public State peek() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public State poll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void offer(State state) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean visited(State state) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public boolean done() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addEdge(Graph<State, Transition> graph, Transition transition) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
